//
//  AppDelegate.h
//  HelloWorld
//
//  Created by Mac on 05.11.13.
//  Copyright (c) 2013 maximusss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
